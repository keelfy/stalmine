package net.sm.gui.blur;

import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.event.EventPriority;
import net.minecraftforge.event.ForgeSubscribe;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;

@SideOnly(Side.CLIENT)
public class overlay extends Gui {
	private Minecraft mc;
	public overlay(Minecraft mc){
		super();
		this.mc = mc;	
	}

	
	@ForgeSubscribe(priority = EventPriority.NORMAL)
	public void eventHandler(RenderGameOverlayEvent event){
		

		
		if(event.isCancelable() || event.type != ElementType.EXPERIENCE){
			
			return;
		}
		
		
		GL11.glDisable(GL11.GL_LIGHTING);
		ScaledResolution sr = new ScaledResolution(Minecraft.getMinecraft().gameSettings, Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight);
		int wight1 = sr.getScaledWidth() /166;
		int height1 = sr.getScaledHeight() /50;
		int wight2 = sr.getScaledWidth() /166;
		int height2 = sr.getScaledHeight() /20;
		drawString(Minecraft.getMinecraft().fontRenderer,EnumChatFormatting.GOLD +  "STALMINE BETA CLIENT", wight1 / 2, height1 - 5, -65536);
		drawString(Minecraft.getMinecraft().fontRenderer,EnumChatFormatting.YELLOW.toString() +  "ВЕРСИЯ [0.0.0.1]", wight2 / 2, height2 - 5, -65536);
		
		
	}
	
	
	
	
	
	}
	

