package net.sm.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.sm.tabs.registrtab;

public class rz5 extends Block {
	
	public rz5(int par1)
    {
        super(par1, Material.glass); 
        this.setCreativeTab(registrtab.tabblck);
        setTextureName("mercury:simple/rz5");
    }
	
    public boolean isOpaqueCube() 
    {
    	return false;
    }
	
}