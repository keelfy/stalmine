package net.sm.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.util.Icon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.sm.render.invincible;
import net.sm.tabs.registrtab;

public class invinciblebase extends Block {

	public Icon[] iconArray;

	
	
	
	public invinciblebase(int par1)
    {
        super(par1, Material.glass); 
        this.setCreativeTab(registrtab.tabblck);
        this.setHardness(10000.0F);
        this.setResistance(20000.0F);
        setStepSound(Block.soundStoneFootstep);
        this.setLightOpacity(0);
    }

	   public Icon getIcon(int par1, int par2) {
	      return !Minecraft.getMinecraft().thePlayer.capabilities.isCreativeMode?this.iconArray[0]:this.iconArray[1];
	   }
	
    public boolean isOpaqueCube() 
    {
    	return false;
    }
    
   

	


		public boolean shouldSideBeRendered(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5)
	    {
			
			if(Minecraft.getMinecraft().thePlayer.capabilities.isCreativeMode){
				invincible.invincible = true;
			}
			
			else{
				invincible.invincible = false;
			}
			
		
			
			return invincible.invincible;
		
	    }
		
		@Override
	    public MovingObjectPosition collisionRayTrace(World par1World, int par2, int par3, int par4, Vec3 par5Vec3, Vec3 par6Vec3)
	    {
			if(invincible.invincible) {
				return super.collisionRayTrace(par1World, par2, par3, par4, par5Vec3, par6Vec3);
			}
			else {
				return null;
			}
	    }
		
		public int quantityDropped(Random par1Random)
	    {
	        return 0;
	    }

		
}