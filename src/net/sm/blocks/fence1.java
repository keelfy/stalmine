package net.sm.blocks;


import net.minecraft.block.BlockPane;
import net.minecraft.block.material.Material;
import net.sm.tabs.registrtab;

public class fence1 extends BlockPane {
  public fence1(String texture, Material material) {
    super(3002, "mercury:simple/fence1","mercury:simple/fence1", material.iron, false);
    this.setHardness(10000.0F);
    this.setResistance(20000.0F);
    setCreativeTab(registrtab.tabblck);
  }
}