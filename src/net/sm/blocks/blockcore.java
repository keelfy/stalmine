package net.sm.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.sm.core.stalminecore;

public class blockcore {
	
  public static Block fence1;
  public static Block fence2;
  public static Block hromakey;
  public static Block wire;
  public static Block radb1;
  public static Block radb2;
  public static Block radb3;
  public static Block radb4;
  public static Block rz1;
  public static Block rz2;
  public static Block rz3;
  public static Block rz4;
  public static Block rz5;
  public static Block barier;
  public static Block bloodb1;
  public static Block bloodb2;
  public static Block bloodb3;
  public static Block bloodb4;
  public static Block electrob1;
  public static Block electrob2;
  public static Block electrob3;
  public static Block electrob4;
  public static Block psib1;
  public static Block psib2;
  public static Block psib3;
  public static Block psib4;
  public static Block termb1;
  public static Block termb2;
  public static Block termb3;
  public static Block termb4;
  public static Block blockname;

  public static void loadBlocks() {
	  
	  long start = System.currentTimeMillis();
	  

	  fence1 = new fence1("mercury:simple/fence1", Material.iron).setHardness(5.0F).setResistance(10.0F).setUnlocalizedName("fence1");
      GameRegistry.registerBlock(fence1);

	  fence2 = new fence2("mercury:simple/fence2", Material.iron).setHardness(5.0F).setResistance(10.0F).setUnlocalizedName("fence2");
	  GameRegistry.registerBlock(fence2);
    
	  hromakey = new hromakey(3000).setUnlocalizedName("hromakey");
	  GameRegistry.registerBlock(hromakey);
	  
	  wire = new wire(3001).setUnlocalizedName("wire");
	  GameRegistry.registerBlock(wire);
	  
	  rz1 = new rz1(3008).setUnlocalizedName("rz1");
	  GameRegistry.registerBlock(rz1);

	  rz2 = new rz2(3009).setUnlocalizedName("rz2");
	  GameRegistry.registerBlock(rz2);
	  
	  rz3 = new rz3(3010).setUnlocalizedName("rz3");
	  GameRegistry.registerBlock(rz3);
	  
	  rz4 = new rz4(3011).setUnlocalizedName("rz4");
	  GameRegistry.registerBlock(rz4);
	  
	  rz5 = new rz5(3012).setUnlocalizedName("rz5");
	  GameRegistry.registerBlock(rz5);
	  
	  barier = new barier(3013).setUnlocalizedName("barier");
	  GameRegistry.registerBlock(barier);
	  
	  bloodb1 = new bloodb1(3014).setUnlocalizedName("bloodb1");
	  GameRegistry.registerBlock(bloodb1);
	  
	  bloodb2 = new bloodb2(3015).setUnlocalizedName("bloodb2");
	  GameRegistry.registerBlock(bloodb2);
	  
	  bloodb3 = new bloodb3(3016).setUnlocalizedName("bloodb3");
	  GameRegistry.registerBlock(bloodb3);
	  
	  bloodb4 = new bloodb4(3017).setUnlocalizedName("bloodb4");
	  GameRegistry.registerBlock(bloodb4);
	  
	  electrob1 = new electrob1(3018).setUnlocalizedName("electrob1");
	  GameRegistry.registerBlock(electrob1);
	  
	  electrob2 = new electrob2(3019).setUnlocalizedName("electrob2");
	  GameRegistry.registerBlock(electrob2);
	  
	  electrob3 = new electrob3(3020).setUnlocalizedName("electrob3");
	  GameRegistry.registerBlock(electrob3);
	  
	  electrob4 = new electrob4(3021).setUnlocalizedName("electrob4");
	  GameRegistry.registerBlock(electrob4);
	  
	  psib1 = new psib1(3022).setUnlocalizedName("psib1");
	  GameRegistry.registerBlock(psib1);
	  
	  psib2 = new psib2(3023).setUnlocalizedName("psib2");
	  GameRegistry.registerBlock(psib2);
	  
	  psib3 = new psib3(3024).setUnlocalizedName("psib3");
	  GameRegistry.registerBlock(psib3);
	  
	  psib4 = new psib4(3025).setUnlocalizedName("psib4");
	  GameRegistry.registerBlock(psib4);
	  
	  termb1 = new termb1(3026).setUnlocalizedName("termb1");
	  GameRegistry.registerBlock(termb1);
	  
	  termb2 = new termb2(3027).setUnlocalizedName("termb2");
	  GameRegistry.registerBlock(termb2);
	  
	  termb3 = new termb3(3028).setUnlocalizedName("termb3");
	  GameRegistry.registerBlock(termb3);
	  
	  termb4 = new termb4(3029).setUnlocalizedName("termb4");
	  GameRegistry.registerBlock(termb4);
	  
	  radb1 = new radb1(3004).setUnlocalizedName("radb1");
	  GameRegistry.registerBlock(radb1);
	  
	  radb2 = new radb2(3005).setUnlocalizedName("radb2");
	  GameRegistry.registerBlock(radb2);
	  
	  radb3 = new radb3(3006).setUnlocalizedName("radb3");
	  GameRegistry.registerBlock(radb3);
	  
	  radb4 = new radb4(3007).setUnlocalizedName("radb4");
	  GameRegistry.registerBlock(radb4);
		 
	  LanguageRegistry.addName(fence1, "Забор 1");
	  LanguageRegistry.addName(fence2, "Забор 2");
	  LanguageRegistry.addName(hromakey, "Хромакей");
	  LanguageRegistry.addName(wire, "Колючая проволка");
	  LanguageRegistry.addName(radb1, "Невидимый Блок радиации 1");
	  LanguageRegistry.addName(radb2, "Невидимый Блок радиации 2");
	  LanguageRegistry.addName(radb3, "Невидимый Блок радиации 3");
	  LanguageRegistry.addName(radb4, "Невидимый Блок радиации 4");
	  LanguageRegistry.addName(rz1, "Ржавый блок 1");
	  LanguageRegistry.addName(rz2, "Ржавый блок 2");
	  LanguageRegistry.addName(rz3, "Ржавый блок 3");
	  LanguageRegistry.addName(rz4, "Ржавый блок 4");
	  LanguageRegistry.addName(rz5, "Ржавый блок 5");
	  LanguageRegistry.addName(barier, "Невидимая стенка");
	  LanguageRegistry.addName(bloodb1, "Невидимый блок крови 1");
	  LanguageRegistry.addName(bloodb2, "Невидимый блок крови 2");
	  LanguageRegistry.addName(bloodb3, "Невидимый блок крови 3");
	  LanguageRegistry.addName(bloodb4, "Невидимый блок крови 4");
	  LanguageRegistry.addName(electrob1, "Невидимый блок электричества 1");
	  LanguageRegistry.addName(electrob2, "Невидимый блок электричества 2");
	  LanguageRegistry.addName(electrob3, "Невидимый блок электричества 3");
	  LanguageRegistry.addName(electrob4, "Невидимый блок электричества 4");
	  LanguageRegistry.addName(psib1, "Невидимый блок пси 1");
	  LanguageRegistry.addName(psib2, "Невидимый блок пси 2");
	  LanguageRegistry.addName(psib3, "Невидимый блок пси 3");
	  LanguageRegistry.addName(psib4, "Невидимый блок пси 4");
	  LanguageRegistry.addName(termb1, "Невидимый блок термальности 1");
	  LanguageRegistry.addName(termb2, "Невидимый блок термальности 2");
	  LanguageRegistry.addName(termb3, "Невидимый блок термальности 3");
	  LanguageRegistry.addName(termb4, "Невидимый блок термальности 4");
	 
		 
	        

	     
	  long finish = System.currentTimeMillis();
	  long timeConsumedMillis = finish - start;
	  stalminecore.log("Blocks loaded with" + " " + timeConsumedMillis + " milliseconds");
	  
  }
}