package net.sm.blocks;

import net.minecraft.block.BlockPane;
import net.minecraft.block.material.Material;
import net.sm.tabs.registrtab;

public class fence2 extends BlockPane {
  public fence2(String texture, Material material) {
    super(3003, "mercury:simple/fence2","mercury:simple/fence2", material.iron, false);
    this.setHardness(10000.0F);
    this.setResistance(20000.0F);
    setCreativeTab(registrtab.tabblck);
  }
}