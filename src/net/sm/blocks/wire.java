package net.sm.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.sm.tabs.registrtab;

public class wire extends Block {
	
  

	public wire(int par1)
    {
        super(par1, Material.glass); 
        this.setCreativeTab(registrtab.tabblck);
        setTextureName("mercury:simple/wire");
        this.setHardness(10000.0F);
        this.setResistance(20000.0F);
    }
    
	
	
	 public void onEntityCollidedWithBlock(World par1World, int x, int y, int z, Entity par5Entity) {
        par1World.getBlockMetadata(x, y, z);
        if(par5Entity instanceof EntityLivingBase) {
        	par5Entity.setInWeb();
        	par5Entity.attackEntityFrom(DamageSource.generic, 4);
         //  ((EntityLivingBase)par5Entity).addPotionEffect(new PotionEffect(Potion.poison.id, 200, 2));
          // par5Entity.playSound("mercury:pills", 1F, 1F);
          
        }

     }
    public int getRenderType()
    {
        return 1;
    }
    
    public boolean renderAsNormalBlock()
    {
        return false;
    }
    
    public boolean isOpaqueCube() {
    	
    	return false;
    	}
    
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        return null;
    }
    



    
}