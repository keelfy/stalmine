package net.sm.anomaly;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.sm.core.stalminecore;

public class anomalycore {
	
  public static Block kisel;
  public static Block electra;
  public static Block voronka1;
  public static Block voronka2;
  public static Block par;
  public static Block jarka;

  public static void loadAnom() {
	  
	  long start = System.currentTimeMillis();
	  

	  kisel = new kisel(3030).setUnlocalizedName("kisel");
	  GameRegistry.registerBlock(kisel);
	  
	  electra = (new electra(3031)).setUnlocalizedName("electra");
	 GameRegistry.registerBlock(electra);
	 
	 voronka1 = (new voronka1(3032)).setUnlocalizedName("voronka");
	 GameRegistry.registerBlock(voronka1);
	 
	 voronka2 = (new voronka2(3033)).setUnlocalizedName("voronka");
	 GameRegistry.registerBlock(voronka2);
	 
	 jarka = (new jarka(3034)).setUnlocalizedName("jarka");
	 GameRegistry.registerBlock(jarka);
	 
	 par = (new par(3035)).setUnlocalizedName("par");
	 GameRegistry.registerBlock(par);
	 
	 
      LanguageRegistry.addName(electra, "Аномалия 'Электра'");
	  LanguageRegistry.addName(kisel, "Аномалия 'Кисель'");
	  LanguageRegistry.addName(voronka1, "Аномалия 'Воронка'");
	  LanguageRegistry.addName(jarka, "Аномалия 'Жарка'");
	  LanguageRegistry.addName(par, "Аномалия 'DJYM,'");


	     
	  long finish = System.currentTimeMillis();
	  long timeConsumedMillis = finish - start;
	  stalminecore.log("Anomalies loaded with" + " " + timeConsumedMillis + " milliseconds");
	  
  }
}