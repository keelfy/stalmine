package net.sm.anomaly;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import net.sm.particles.voronka;
import net.sm.tabs.registrtab;

public class voronka1 extends Block {
	
	public voronka1(int par1) {
        super(par1, Material.glass); 
        this.setCreativeTab(registrtab.tabanom);
        this.setHardness(10000.0F);
        this.setResistance(20000.0F);
        setStepSound(Block.soundStoneFootstep);
        this.setLightOpacity(0);
        setTextureName("mercury:simple/empty");
      this.setBlockBounds(0.0F, 0.0F, 0.5F, 1F, 2F, 0.5F);
		
	
    }
	
	
    
    public boolean isOpaqueCube() {
    	
    	return false;
    	}
    

    
    public void onEntityCollidedWithBlock(World par1World, int x, int y, int z, Entity par5Entity) {
   	         par1World.getBlockMetadata(x, y, z);
   	        if(par5Entity instanceof EntityLivingBase) {
   	        ((EntityLivingBase)par5Entity).addPotionEffect(new PotionEffect(31, 20, 0));
   	    
   	           
   	   }
 
   	      if(!par1World.isRemote && par1World.rand.nextFloat() > 0.95F && par5Entity instanceof EntityLivingBase && !par5Entity.isEntityInvulnerable()) {
  	          par1World.playSoundAtEntity(par5Entity, "mercury:kiselh", 1.0F, 1.0F);
  	       }
   	        
        }
   	 
   	    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4) {
   	        return null;
    
         }
   	    @SideOnly(Side.CLIENT)
	    public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random) {
	    	
	        if(par1World.rand.nextFloat() < 0.005F) {
	        	
	           par1World.playSound((double)par2, (double)par3, (double)par4, "mercury:kisel", 0.5F + par1World.rand.nextFloat() * 0.5F, 0.9F + par5Random.nextFloat() * 0.15F, false);
	           
	        }
	        
	        int per = 0;
	        per++;
	        
	        if (per == 2) {
	        float f7 = (float)par2 + 0.5F;
	        float f2 = (float)par3 + 1.0F;
	        float f3 = (float)par4 + 0.5F;
	      
	        
	  
	        
	        Minecraft.getMinecraft().effectRenderer.addEffect(new voronka(par1World, (double)(f7), (double)f2, (double)(f3)));
	        }
	    }
   	    

   	    
}