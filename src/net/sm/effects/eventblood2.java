package net.sm.effects;

import net.minecraft.util.DamageSource;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.sm.core.stalminecore;

public class eventblood2 {

    @ForgeSubscribe 
    public void onEntityUpdate(LivingUpdateEvent event) {
        if (event.entityLiving.isPotionActive(stalminecore.blood2)) { 

        	if(event.entityLiving.getActivePotionEffect(stalminecore.blood2).getDuration() == 0){
        		event.entityLiving.removePotionEffect(stalminecore.blood2.id);
        		return;
        	}
        		
        	if(event.entityLiving.getActivePotionEffect(stalminecore.blood2).getDuration() == 500){
        		event.entityLiving.playSound("mercury:heart", 1F, 1F);
        	}
        	
        	if(event.entityLiving.getActivePotionEffect(stalminecore.blood2).getDuration() == 400){
        		event.entityLiving.playSound("mercury:heart", 1F, 1F);
        	}
        	
        	if(event.entityLiving.getActivePotionEffect(stalminecore.blood2).getDuration() == 300){
        		event.entityLiving.playSound("mercury:heart", 1F, 1F);
        	}
        	
        	if(event.entityLiving.getActivePotionEffect(stalminecore.blood2).getDuration() == 200){
        		event.entityLiving.playSound("mercury:heart", 1F, 1F);
        	}
        	
        	if(event.entityLiving.getActivePotionEffect(stalminecore.blood2).getDuration() == 100){
        		event.entityLiving.playSound("mercury:heart", 1F, 1F);
        	}
        	
        	
        		else if(event.entityLiving.worldObj.rand.nextInt(15) == 0){
        			event.entityLiving.attackEntityFrom(DamageSource.generic, 1);
        		
        			
        		}
        	
      
                        
        }
        }
}