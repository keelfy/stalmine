package net.sm.effects;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.sm.core.stalminecore;

public class eventpsy4 {

    @ForgeSubscribe 
    public void onEntityUpdate(LivingUpdateEvent event) {
        if (event.entityLiving.isPotionActive(stalminecore.psy4)) { 

        	if(event.entityLiving.getActivePotionEffect(stalminecore.psy4).getDuration() == 0){
        		event.entityLiving.removePotionEffect(stalminecore.psy4.id);
        		return;
        	}
        	
           	if (event.entity instanceof EntityPlayer) {
        	    EntityPlayer player = (EntityPlayer) event.entity;
        	}
        	
        	
        	if ((event.entityLiving.getActivePotionEffect(stalminecore.psy4).getDuration() % 100) == 0) {
        	    event.entityLiving.playSound("mercury:psy", 1F, 1F);
        	}
        	
        	
        		else if(event.entityLiving.worldObj.rand.nextInt(30) == 0){
        			event.entityLiving.attackEntityFrom(DamageSource.generic, 4);
        			}
                
        }
        }
}