package net.sm.effects;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.sm.core.stalminecore;

public class eventpsy2 {

    @ForgeSubscribe 
    public void onEntityUpdate(LivingUpdateEvent event) {
        if (event.entityLiving.isPotionActive(stalminecore.psy2)) { 

        	if(event.entityLiving.getActivePotionEffect(stalminecore.psy2).getDuration() == 0){
        		event.entityLiving.removePotionEffect(stalminecore.psy2.id);
        		return;
        	}
        	
           	if (event.entity instanceof EntityPlayer) {
        	    EntityPlayer player = (EntityPlayer) event.entity;
        	}
        	
        	
        	if ((event.entityLiving.getActivePotionEffect(stalminecore.psy2).getDuration() % 100) == 0) {
        	    event.entityLiving.playSound("mercury:psy", 1F, 1F);
        	}
        	
        		else if(event.entityLiving.worldObj.rand.nextInt(30) == 0){
        			event.entityLiving.attackEntityFrom(DamageSource.generic, 2);
        			}
            
        }
        }
}