package net.sm.tileentity;


import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;

public abstract class tileanom extends TileEntity {

   public List entities = new ArrayList();
   public int activeTimer = 0;


   public void updateEntity() {
      super.updateEntity();
      if(super.worldObj.isRemote) {
         if(this.activeTimer > 0) {
            --this.activeTimer;
            this.setActive();
         } else {
            this.setStatic();
         }
      }

      AxisAlignedBB aabb = AxisAlignedBB.getBoundingBox((double)(super.xCoord - 1), (double)super.yCoord - 1.5D, (double)(super.zCoord - 1), (double)(super.xCoord + 2), (double)super.yCoord + 1.5D, (double)(super.zCoord + 2));
      this.entities = super.worldObj.getEntitiesWithinAABB(EntityLivingBase.class, aabb);
      Iterator i$ = this.entities.iterator();

      while(i$.hasNext()) {
         Entity entity = (Entity)i$.next();
         if(entity.getDistance((double)super.xCoord + 0.5D, (double)super.yCoord + (entity.posY - entity.boundingBox.minY), (double)super.zCoord + 0.5D) < this.setDistanceActive()) {
            this.activeTimer = this.setActiveTimer();
            this.setHitEntity(entity);
         }
      }

   }


   protected abstract int setActiveTimer();

   protected abstract double setDistanceActive();

   protected abstract void setHitEntity(Entity var1);

   protected abstract void setActive();

   protected abstract void setStatic();

   public boolean canUpdate() {
      return FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT;
   }
}
