package net.sm.items;

import java.util.List;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.sm.tabs.registrtab;
 
public class rub10 extends Item
{
public rub10(int par1, int par2, float par3, boolean par4)
{
super(par1);
setCreativeTab(registrtab.tabmon);
this.maxStackSize = 64;
}


//ICONKA
public void registerIcons(IconRegister par1IconRegister)
{
this.itemIcon = par1IconRegister.registerIcon("mercury:item/rub10");
}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{
	 par3List.add(EnumChatFormatting.YELLOW + "Денежная купюра номиналом в десять");
	 par3List.add(EnumChatFormatting.YELLOW + "рублей.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
	 par3List.add(EnumChatFormatting.BLUE + "Раритетность:" + EnumChatFormatting.GRAY  + " ОБЫЧНЫЙ");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.0 КГ");
}


}