package net.sm.items;

import java.util.List;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.sm.tabs.registrtab;

public class svoboda extends Item
{
public svoboda(int par1, int par2, float par3, boolean par4)
{
super(par1);
setCreativeTab(registrtab.tabitms);
this.maxStackSize = 1;
}


//ICONKA
public void registerIcons(IconRegister par1IconRegister)
{
this.itemIcon = par1IconRegister.registerIcon("mercury:item/svoboda");
}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{
	 par3List.add(EnumChatFormatting.YELLOW + "Специальная нашивка, подтверждающая");
	 par3List.add(EnumChatFormatting.YELLOW + "вашу преданность 'Свободе'.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
	 par3List.add(EnumChatFormatting.BLUE + "Раритетность:" + EnumChatFormatting.RED  + " СПЕЦИАЛЬНЫЙ");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.1 КГ");
}
}