package net.sm.artefacts;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.sm.tabs.registrtab;

public class pruj extends Item {
	
	public pruj(int par1){
		
		super(par1);
		setMaxStackSize(1);
		setCreativeTab(registrtab.tabart);
		setTextureName("mercury:artefacts/pruj");
		
	}

    public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5) {
    	
    super.onUpdate(par1ItemStack, par2World, par3Entity, par4, par5);
    EntityLivingBase ent = (EntityLivingBase)par3Entity;
    
    ent.addPotionEffect(new PotionEffect(11, 20, 0));
    
    }	
    
    
  //OPISANIE
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
    {

    	 par3List.add(EnumChatFormatting.YELLOW + "'Геркулес' — стимулирующий препарат,");
    	 par3List.add(EnumChatFormatting.YELLOW + "уменьшающий утомляемость мышц. Это по-");
    	 par3List.add(EnumChatFormatting.YELLOW + "могает спытывать большие нагрузки с ме-");
    	 par3List.add(EnumChatFormatting.YELLOW + "ньшим расходом силы и энергии.Использу-");
    	 par3List.add(EnumChatFormatting.YELLOW + "ется в основном Сталкерами и Учеными.");
    	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
         par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.DARK_AQUA + " РЕДКИЙ");
         par3List.add(EnumChatFormatting.BLUE + "Дполнительный вес +20 на 2 мин.");
    	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.1 КГ");
    }
    
}