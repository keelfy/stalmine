package net.sm.tabs;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.sm.core.stalminecore;

public class tabitms extends CreativeTabs
{
 public tabitms(int position, String tabID)
  {
    super(position, tabID);
  }

 @SideOnly(Side.CLIENT)
  public int getTabIconItemIndex()
  {
   return stalminecore.comp.itemID;
 }

 public String getTranslatedTabLabel()
  {
   return "Специальные предметы";
  }
}
