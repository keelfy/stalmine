package net.sm.tabs;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.sm.core.stalminecore;

public class tabdecor extends CreativeTabs
{
 public tabdecor(int position, String tabID)
  {
    super(position, tabID);
  }

 @SideOnly(Side.CLIENT)
  public int getTabIconItemIndex()
  {
   return stalminecore.alpen1.itemID;
 }

 public String getTranslatedTabLabel()
  {
   return "Специальный декор";
  }
}
