package net.sm.tabs;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.sm.anomaly.anomalycore;

public class tabanom extends CreativeTabs
{
 public tabanom(int position, String tabID)
  {
    super(position, tabID);
  }
@Override
 @SideOnly(Side.CLIENT)
  public int getTabIconItemIndex()
  {
   return anomalycore.kisel.blockID;
 }

 public String getTranslatedTabLabel()
  {
   return "Аномалии";
  }
}



