package net.sm.tabs;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.sm.blocks.blockcore;

public class tabblck extends CreativeTabs
{
 public tabblck(int position, String tabID)
  {
    super(position, tabID);
  }

 @SideOnly(Side.CLIENT)
  public int getTabIconItemIndex()
  {
   return blockcore.barier.blockID;
 }

 public String getTranslatedTabLabel()
  {
   return "Специальные блоки";
  }
}
