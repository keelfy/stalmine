package net.sm.tabs;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.sm.core.stalminecore;

public class tabmon extends CreativeTabs
{
 public tabmon(int position, String tabID)
  {
    super(position, tabID);
  }

 @SideOnly(Side.CLIENT)
  public int getTabIconItemIndex()
  {
   return stalminecore.rub5000.itemID;
 }

 public String getTranslatedTabLabel()
  {
   return "Денежная валюта";
  }
}
