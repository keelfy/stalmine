package net.sm.tabs;

 import cpw.mods.fml.relauncher.Side;
 import cpw.mods.fml.relauncher.SideOnly;
 import net.sm.core.stalminecore;
 import net.minecraft.creativetab.CreativeTabs;

public class tabfood extends CreativeTabs
{
  public tabfood(int position, String tabID)
   {
     super(position, tabID);
   }

  @SideOnly(Side.CLIENT)
   public int getTabIconItemIndex()
   {
    return stalminecore.pizza.itemID;
  }

  public String getTranslatedTabLabel()
   {
    return "Провизия";
   }
 }
