package net.sm.medkits;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.sm.core.stalminecore;
import net.sm.tabs.registrtab;

     public class antirad3 extends Item
     {
    	
    	 
//REGISTR
public antirad3(int par1, int par2, float par3, boolean par4)
{
super(par1);
setCreativeTab(registrtab.tabmed);
this.maxStackSize = 64;
}


//ICONKA
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:medkits/antirad3");}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "Набор таблеток с активными вещества-");
	 par3List.add(EnumChatFormatting.YELLOW + "ми способствующими выведению радиону-");
	 par3List.add(EnumChatFormatting.YELLOW + "клидов из организма.При применении вы-");
	 par3List.add(EnumChatFormatting.YELLOW + "зывает сужение периферических кровено-");
	 par3List.add(EnumChatFormatting.YELLOW + "сных сосудов и кислородное голодание,");
	 par3List.add(EnumChatFormatting.YELLOW + "изготавливается группировкой 'Ученые'.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
   par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.LIGHT_PURPLE + " ЛЕГЕНДАРНЫЙ");
	 par3List.add(EnumChatFormatting.BLUE + "Вывод радиации 75%");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.1 КГ");
}



//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//FUNC  
public ItemStack onItemRightClick(ItemStack is, World world, EntityPlayer player)
      {
	
//ZVUK
         world.playSoundAtEntity(player, "mercury:pills", 1F, 1F);
     
         
//REMOVE POTION
         if(!world.isRemote) {
    		 player.removePotionEffect(stalminecore.rad1.id);
    		 player.removePotionEffect(stalminecore.rad2.id);
    		 player.removePotionEffect(stalminecore.rad3.id);
		 }
         
         
  player.inventory.consumeInventoryItem(stalminecore.antirad3.itemID);

    return is;
   }


 }
     
     
     