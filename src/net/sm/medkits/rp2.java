package net.sm.medkits;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.sm.core.stalminecore;
import net.sm.tabs.registrtab;

     public class rp2 extends Item
     {
    	
    	 
//REGISTR
public rp2(int par1, int par2, float par3, boolean par4)
{
super(par1);
setCreativeTab(registrtab.tabmed);
this.maxStackSize = 64;
}


//ICONKA
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:medkits/rp2");}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "'Индралин-Б220' входил в состав лич-");
	 par3List.add(EnumChatFormatting.YELLOW + "ных аптечек работников АЭС.Уменьшает");
	 par3List.add(EnumChatFormatting.YELLOW + "воздействие гамма-излучения на ткани");
	 par3List.add(EnumChatFormatting.YELLOW + "живого организма. Данный препарат же-");
	 par3List.add(EnumChatFormatting.YELLOW + "лательно применять до облучения.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
    par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.DARK_PURPLE + " МИФИЧЕСКИЙ");
	 par3List.add(EnumChatFormatting.BLUE + "Защита от радиации 50% на 15 сек.");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.1 КГ");
}



//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//FUNC  
public ItemStack onItemRightClick(ItemStack is, World world, EntityPlayer player)
      {
	
//ZVUK
         world.playSoundAtEntity(player, "mercury:pills", 1F, 1F);
     
         
//REMOVE POTION
         if(!world.isRemote) {
        	 player.addPotionEffect(new PotionEffect(56, 300, 0));
		 }
         
         
  player.inventory.consumeInventoryItem(stalminecore.rp2.itemID);

    return is;
   }


 }
     
     
     