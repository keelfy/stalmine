package net.sm.medkits;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.sm.core.stalminecore;
import net.sm.tabs.registrtab;

     public class barv4 extends Item
     {
    	
    	 
//REGISTR
public barv4(int par1, int par2, float par3, boolean par4)
{
super(par1);
setCreativeTab(registrtab.tabmed);
this.maxStackSize = 64;
}


//ICONKA
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:medkits/barv4");}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "Синтетический аналог витамина К. Осн-");
	 par3List.add(EnumChatFormatting.YELLOW + "овное действие заключается в повышении-");
	 par3List.add(EnumChatFormatting.YELLOW + "свёртываемости крови.Изготавливается гру-");
	 par3List.add(EnumChatFormatting.YELLOW + "ппировкой Ученые, редкий, позволяет о-");
	 par3List.add(EnumChatFormatting.YELLOW + "рганизму быстрее рубцевать рваные раны.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
     par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.GOLD + " БЕСЦЕННЫЙ");
	 par3List.add(EnumChatFormatting.BLUE + "Свертываемость 100% на 1 мин.");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.1 КГ");
}



//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//FUNC  
public ItemStack onItemRightClick(ItemStack is, World world, EntityPlayer player)
      {
	
//ZVUK
         world.playSoundAtEntity(player, "mercury:pills", 1F, 1F);
     
         
//REMOVE POTION
         if(!world.isRemote) {
        	 player.addPotionEffect(new PotionEffect(62, 1200, 0));
		 }
         
         
  player.inventory.consumeInventoryItem(stalminecore.barv4.itemID);

    return is;
   }


 }
     
     
     