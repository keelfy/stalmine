package net.sm.medkits;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.sm.core.stalminecore;
import net.sm.tabs.registrtab;

public class bint4 extends Item
     {
    	
    	 
//REGISTR
public bint4(int par1, int par2, float par3, boolean par4)
{
super(par1);
setCreativeTab(registrtab.tabmed);
this.maxStackSize = 64;
}


//ICONKA
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:medkits/bint4");}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "Асептический бинт для остановки кровоте-");
	 par3List.add(EnumChatFormatting.YELLOW + "чения и предотвращения попадания в рану");
	 par3List.add(EnumChatFormatting.YELLOW + "инфекции.Позволяет перевезать вены и арт-");
	 par3List.add(EnumChatFormatting.YELLOW + "ерии и используется в дефицитной аптечке");
	 par3List.add(EnumChatFormatting.YELLOW + "для первой помощи.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
    par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.GOLD + " БЕСЦЕННЫЙ");
	 par3List.add(EnumChatFormatting.BLUE + "Заживление ран 100%");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.1 КГ");
}



//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//FUNC  
public ItemStack onItemRightClick(ItemStack is, World world, EntityPlayer player)
      {
	
//ZVUK
         world.playSoundAtEntity(player, "mercury:bandage", 1F, 1F);
     
         
//REMOVE POTION
         if(!world.isRemote) {
         player.removePotionEffect(stalminecore.blood1.id);
         player.removePotionEffect(stalminecore.blood2.id);
    	 player.removePotionEffect(stalminecore.blood3.id);
		 player.removePotionEffect(stalminecore.blood4.id);
		 }
         
         
  player.inventory.consumeInventoryItem(stalminecore.bint4.itemID);

    return is;
   }


 }
     
     
     