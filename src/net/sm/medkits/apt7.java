package net.sm.medkits;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.sm.core.stalminecore;
import net.sm.tabs.registrtab;

public class apt7 extends Item
     {
    	
    	 
//REGISTR
public apt7(int par1, int par2, float par3, boolean par4)
{
super(par1);
setCreativeTab(registrtab.tabmed);
this.maxStackSize = 64;
}


//ICONKA
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:medkits/apt7");}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "Дефицитная универсальный комплект пе-");
	 par3List.add(EnumChatFormatting.YELLOW + "рвой помощи для разового применения на");
	 par3List.add(EnumChatFormatting.YELLOW + "одного человека.Комплект отлично боре-");
	 par3List.add(EnumChatFormatting.YELLOW + "тся с разными видами ран: пулевыми ра-");
	 par3List.add(EnumChatFormatting.YELLOW + "нениями, ожогами, отравлениями и т.д.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
    par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.RED + " СПЕЦИАЛЬНЫЙ");
	 par3List.add(EnumChatFormatting.BLUE + "Восстановления здоровья 100%");
	 par3List.add(EnumChatFormatting.BLUE + "Свертываемость 100% на 5 сек.");
	 par3List.add(EnumChatFormatting.BLUE + "Термозащита 100% на 5 сек.");
	 par3List.add(EnumChatFormatting.BLUE + "Химзащита 100% на на 5 сек.");
	 par3List.add(EnumChatFormatting.BLUE + "Защита от радиации 100% на 5 сек.");
	 par3List.add(EnumChatFormatting.BLUE + "Пси-блокада 100% на 5 сек.");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.2 КГ");
}



//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//FUNC  
public ItemStack onItemRightClick(ItemStack is, World world, EntityPlayer player)
      {
	
//ZVUK
         world.playSoundAtEntity(player, "mercury:medkit", 1F, 1F);
     
         
//REMOVE POTION
         if(!world.isRemote) {
         player.addPotionEffect(new PotionEffect(6, 5, 0));
         player.addPotionEffect(new PotionEffect(50, 100, 0));
         player.addPotionEffect(new PotionEffect(54, 100, 0));
         player.addPotionEffect(new PotionEffect(58, 100, 0));
         player.addPotionEffect(new PotionEffect(62, 100, 0));
         player.addPotionEffect(new PotionEffect(12, 100, 0));
		 }
         
         
  player.inventory.consumeInventoryItem(stalminecore.apt7.itemID);

    return is;
   }


 }
     
     
     