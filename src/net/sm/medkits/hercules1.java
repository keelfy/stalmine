package net.sm.medkits;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.sm.core.stalminecore;
import net.sm.tabs.registrtab;

public class hercules1 extends Item
     {
    	
    	 
//REGISTR
public hercules1(int par1, int par2, float par3, boolean par4)
{
super(par1);
setCreativeTab(registrtab.tabmed);
this.maxStackSize = 64;
}


//ICONKA
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:medkits/hercules1");}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "'Геркулес' — стимулирующий препарат,");
	 par3List.add(EnumChatFormatting.YELLOW + "уменьшающий утомляемость мышц. Это по-");
	 par3List.add(EnumChatFormatting.YELLOW + "могает спытывать большие нагрузки с ме-");
	 par3List.add(EnumChatFormatting.YELLOW + "ньшим расходом силы и энергии.Использу-");
	 par3List.add(EnumChatFormatting.YELLOW + "ется в основном Сталкерами и Учеными.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
     par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.DARK_AQUA + " РЕДКИЙ");
     par3List.add(EnumChatFormatting.BLUE + "Дполнительный вес +20 на 2 мин.");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.1 КГ");
}



//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//FUNC  
public ItemStack onItemRightClick(ItemStack is, World world, EntityPlayer player)
      {
	
//ZVUK
         world.playSoundAtEntity(player, "mercury:pills", 1F, 1F);
     
         
//REMOVE POTION
         if(!world.isRemote) {
		 
		 }
         
         
  player.inventory.consumeInventoryItem(stalminecore.hercules1.itemID);

    return is;
   }


 }
     
     
     