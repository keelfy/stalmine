package net.sm.particles;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_GREATER;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glAlphaFunc;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDepthMask;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;

import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class jar extends EntityFX
{

	private static final ResourceLocation texture = new ResourceLocation("mercury:textures/particle/jar.png");
	  private static float alpha;
      private static boolean faze;
	
	public jar(World par1World, double x, double y, double z) {
		super(par1World, x, y, z, 0.0D, 0.0D, 0.0D);
		setGravity(-1);
		setMaxAge(10);
		
	}
	
	public void renderParticle(Tessellator tess, float ticks, float par3, float par4, float par5, float par6, float par7){
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
		glDepthMask(false);
		glEnable(GL_BLEND);
	
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glAlphaFunc(GL_GREATER, 0.003921569F);
		tess.startDrawingQuads();
		tess.setBrightness(getBrightnessForRender(ticks));
		float scale = 0.2F * particleScale;
		   float x = (float)(prevPosX + (posX - prevPosX) * ticks - interpPosX);
	        float y = (float)(prevPosY + (posY - prevPosY) * ticks - interpPosY);
	        float z = (float)(prevPosZ + (posZ - prevPosZ) * ticks - interpPosZ);
		tess.addVertexWithUV(x - par3 * scale - par6 * scale, y - par4 * scale, z - par5 * scale - par7 * scale, 0, 0);
        tess.addVertexWithUV(x - par3 * scale + par6 * scale, y + par4 * scale, z - par5 * scale + par7 * scale, 1, 0);
        tess.addVertexWithUV(x + par3 * scale + par6 * scale, y + par4 * scale, z + par5 * scale + par7 * scale, 1, 1);
        tess.addVertexWithUV(x + par3 * scale - par6 * scale, y - par4 * scale, z + par5 * scale - par7 * scale, 0, 1);
		tess.draw();
		glDisable(GL_BLEND);
		glDepthMask(true);
		glAlphaFunc(GL_GREATER, 0.1F);
	}
	
	   public void onUpdate()
	    {
	         this.prevPosX = this.posX;
	         this.prevPosY = this.posY;
	         this.prevPosZ = this.posZ;

	         if (this.particleAge++ >= this.particleMaxAge)
	         {
	             this.setDead();
	         }

	         this.motionY -= 0.3D * (double)this.particleGravity;
	         this.moveEntity(this.motionX, this.motionY, this.motionZ);
	         this.motionX *= 0.0D;
	         this.motionY *= 0.000000001D;
	         this.motionZ *= 0.0D;

	         if (this.onGround)
	         {
	             this.motionX *= 0.0D;
	             this.motionZ *= 0.0D;
	         }
	        
	        if(faze) {
	            if(alpha < 1.0F) {
	               alpha += 0.025F;
	            } else {
	               faze = false;
	               alpha = 1.0F;
	            }
	         } else if(alpha > 0.0F) {
	            alpha -= 0.05F;
	         } else {
	            faze = true;
	            alpha = 0.0F;
	         }
	         glColor4f(1.0F, 1.0F, 1.0F, alpha);
	    }

	
	public int getFXLayer(){
		return 3;
	}
	
	public jar setMaxAge(int maxAge){
		particleMaxAge = maxAge;
		return this;
	}
	
	public jar setScale(float scale){
		particleScale = scale;
		return this;
	}
	
	public jar setGravity(float gravity){
		particleGravity = gravity;
		return this;
	}
	

	
	
}