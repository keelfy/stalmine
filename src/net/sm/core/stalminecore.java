package net.sm.core;


import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Metadata;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.item.Item;
import net.minecraft.potion.Potion;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.MinecraftForge;
import net.sm.anomaly.anomalycore;
import net.sm.artefacts.*;
import net.sm.blocks.blockcore;
import net.sm.effects.*;
import net.sm.food.*;
import net.sm.items.*;
import net.sm.medkits.*;

import net.minecraft.world.biome.SpawnListEntry;
import net.sm.proxy.commonproxy;

///////////////////////////////////////////////////////////////////////////network and mod id/////////////////////////////////////////////////////////////////////////////////////////
@Mod(modid = "mercury", name = "S.T.A.L.M.I.N.E Lost Paradise", version = "0.1.0.0")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
///////////////////////////////////////////////////////////////////////////network and mod id////////////////////////////////////////////////////////////////////////////////////////


public class stalminecore {
	
	   @Mod.Instance("mercury")
	    public static stalminecore instance;
	   
	   @Metadata
	   public static ModMetadata meta;
	   
	   
    @SidedProxy(clientSide="clientproxy", serverSide="commonproxy")
   
    public static commonproxy proxy;
    soundengine events = new soundengine();
  
  
///////////////////////////////////////////////////////////////////////////Food/////////////////////////////////////////////////////////////////////////////////////////////////////
	 public static Item alpen1 = new alpen1(2001, 2, 0.4F, false).setUnlocalizedName("alpen1");
	 public static Item alpen2 = new alpen2(2002, 2, 0.4F, false).setUnlocalizedName("alpen2");
	 public static Item alpen3 = new alpen3(2003, 2, 0.4F, false).setUnlocalizedName("alpen3");
	 public static Item ananas = new ananas(2004, 3, 0.4F, false).setUnlocalizedName("ananas");
	 public static Item assorti = new assorti(2005, 8, 0.4F, false).setUnlocalizedName("assorti");
	 public static Item babana = new babana(2006, 3, 0.4F, false).setUnlocalizedName("babana");
	 public static Item bonduelle = new bonduelle(2007, 7, 0.4F, false).setUnlocalizedName("bonduelle");
	 public static Item bonpari1 = new bonpari1(2008, 3, 0.4F, false).setUnlocalizedName("bonpari1");
	 public static Item bonpari2 = new bonpari2(2009, 3, 0.4F, false).setUnlocalizedName("bonpari2");
	 public static Item bonpari3 = new bonpari3(2010, 3, 0.4F, false).setUnlocalizedName("bonpari3");
	 public static Item bonpari4 = new bonpari4(2011, 3, 0.4F, false).setUnlocalizedName("bonpari4");
	 public static Item file1 = new file1(2012, 10, 0.4F, false).setUnlocalizedName("file1");
	 public static Item file2 = new file2(2013, 10, 0.4F, false).setUnlocalizedName("file2");
	 public static Item file3 = new file3(2014, 10, 0.4F, false).setUnlocalizedName("file3");
	 public static Item file4 = new file4(2015, 10, 0.4F, false).setUnlocalizedName("file4");
	 public static Item file5 = new file5(2016, 10, 0.4F, false).setUnlocalizedName("file5");
	 public static Item file6 = new file6(2017, 10, 0.4F, false).setUnlocalizedName("file6");
	 public static Item flyaga = new flyaga(2021, 0, 0.4F, false).setUnlocalizedName("flyaga");
	 public static Item govyadina = new govyadina(2022, 6, 0.4F, false).setUnlocalizedName("govyadina");
	 public static Item hienz = new hienz(2023, 6, 0.4F, false).setUnlocalizedName("hienz");
	 public static Item hleb1 = new hleb1(2024, 2, 0.4F, false).setUnlocalizedName("hleb1");
	 public static Item hleb2 = new hleb2(2025, 4, 0.4F, false).setUnlocalizedName("hleb2");
	 public static Item kilka = new kilka(2026, 7, 0.4F, false).setUnlocalizedName("kilka");
	 public static Item kolbosa1 = new kolbosa1(2027, 7, 0.4F, false).setUnlocalizedName("kolbosa1");
	 public static Item kolbosa2 = new kolbosa2(2028, 7, 0.4F, false).setUnlocalizedName("kolbosa2");
	 public static Item konyak = new konyak(2029, 0, 0.4F, false).setUnlocalizedName("konyak");
	 public static Item mandarin = new mandarin(2030, 2, 0.4F, false).setUnlocalizedName("mandarin");
	 public static Item mre = new mre(2031, 20, 0.4F, false).setUnlocalizedName("mre");
	 public static Item myaso = new myaso(2032, 6, 0.4F, false).setUnlocalizedName("myaso");
	 public static Item ogyrci = new ogyrci(2033, 6, 0.4F, false).setUnlocalizedName("ogyrci");
	 public static Item oreo = new oreo(2034, 3, 0.4F, false).setUnlocalizedName("oreo");
	 public static Item oreobig = new oreobig(2035, 12, 0.4F, false).setUnlocalizedName("oreobig");
	 public static Item pechenie1 = new pechenie1(2036, 4, 0.4F, false).setUnlocalizedName("pechenie1");
	 public static Item pizza = new pizza(2037, 20, 0.4F, false).setUnlocalizedName("pizza");
	 public static Item redbull = new redbull(2038, 0, 0.4F, false).setUnlocalizedName("redbull");
	 public static Item rolton1 = new rolton1(2039, 5, 0.4F, false).setUnlocalizedName("rolton1");
	 public static Item rolton2 = new rolton2(2040, 5, 0.4F, false).setUnlocalizedName("rolton2");
	 public static Item saira = new saira(2041, 8, 0.4F, false).setUnlocalizedName("saira");
	 public static Item sendwich = new sendwich(2042, 12, 0.4F, false).setUnlocalizedName("sendwich");
	 public static Item shproti = new shproti(2043, 8, 0.4F, false).setUnlocalizedName("shproti");
	 public static Item sok = new sok(2044, 0, 0.4F, false).setUnlocalizedName("sok");
	 public static Item spam = new spam(2045, 10, 0.4F, false).setUnlocalizedName("spam");
	 public static Item sufle1 = new sufle1(2046, 6, 0.4F, false).setUnlocalizedName("sufle1");
	 public static Item sufle2 = new sufle2(2047, 6, 0.4F, false).setUnlocalizedName("sufle2");
	 public static Item sufle3 = new sufle3(2048, 6, 0.4F, false).setUnlocalizedName("sufle3");
	 public static Item sufle4 = new sufle4(2049, 6, 0.4F, false).setUnlocalizedName("sufle4");
	 public static Item tynec = new tynec(2050, 8, 0.4F, false).setUnlocalizedName("tynec");
	 public static Item voda = new voda(2051, 0, 0.4F, false).setUnlocalizedName("voda");
	 public static Item vodka = new vodka(2052, 0, 0.4F, false).setUnlocalizedName("vodka");
	 public static Item yabloko = new yabloko(2053, 2, 0.4F, false).setUnlocalizedName("yabloko"); 
///////////////////////////////////////////////////////////////////////////Food/////////////////////////////////////////////////////////////////////////////////////////////////////
	 
	 
	 
///////////////////////////////////////////////////////////////////////////Item/////////////////////////////////////////////////////////////////////////////////////////////////////
	 public static Item bandit = new bandit(2054, 0, 0.0F, false).setUnlocalizedName("bandit");
	 public static Item dolg = new dolg(2055, 0, 0.0F, false).setUnlocalizedName("dolg");
	 public static Item military = new military(2056, 0, 0.0F, false).setUnlocalizedName("military");
	 public static Item monolith = new monolith(2057, 0, 0.0F, false).setUnlocalizedName("monolith");
	 public static Item naemnik = new naemnik(2058, 0, 0.0F, false).setUnlocalizedName("naemnik");
	 public static Item stalker = new stalker(2059, 0, 0.0F, false).setUnlocalizedName("stalker");
	 public static Item svoboda = new svoboda(2060, 0, 0.0F, false).setUnlocalizedName("svoboda");
	 public static Item comp = new comp(2061, 0, 0.0F, false).setUnlocalizedName("comp"); 
	 public static Item rub10 = new rub10(2062, 0, 0.0F, false).setUnlocalizedName("rub10");
	 public static Item rub100 = new rub100(2063, 0, 0.0F, false).setUnlocalizedName("rub100");
	 public static Item rub1000 = new rub1000(2064, 0, 0.0F, false).setUnlocalizedName("rub1000");
	 public static Item rub100s = new rub100s(2065, 0, 0.0F, false).setUnlocalizedName("rub100s");
	 public static Item rub50 = new rub50(2066, 0, 0.0F, false).setUnlocalizedName("rub50");
	 public static Item rub500 = new rub500(2067, 0, 0.0F, false).setUnlocalizedName("rub500");
	 public static Item rub5000 = new rub5000(2068, 0, 0.0F, false).setUnlocalizedName("rub5000");
	 public static Item rub1 = new rub1(2069, 0, 0.0F, false).setUnlocalizedName("rub1");
	 public static Item rub2 = new rub2(2070, 0, 0.0F, false).setUnlocalizedName("rub2");
	 public static Item rub5 = new rub5(2071, 0, 0.0F, false).setUnlocalizedName("rub5");
///////////////////////////////////////////////////////////////////////////Item/////////////////////////////////////////////////////////////////////////////////////////////////////
	 
	 
///////////////////////////////////////////////////////////////////////////Block/////////////////////////////////////////////////////////////////////////////////////////////////////
	// public static final Block hromakey = new hromakey(3000).setUnlocalizedName("hromakey");
	// public static final Block wire = new wire(3001).setUnlocalizedName("wire");
	// public static final Block radb1 = new radb1(3004).setUnlocalizedName("radb1");
	// public static final Block radb2 = new radb2(3005).setUnlocalizedName("radb2");
	// public static final Block radb3 = new radb3(3006).setUnlocalizedName("radb3");
	// public static final Block radb4 = new radb4(3007).setUnlocalizedName("radb4");
///////////////////////////////////////////////////////////////////////////Block/////////////////////////////////////////////////////////////////////////////////////////////////////
	 
	 
///////////////////////////////////////////////////////////////////////////Effects//////////////////////////////////////////////////////////////////////////////////////////////////
	 public static Potion blood1;
	 public static Potion blood2;
	 public static Potion blood3;
	 public static Potion blood4;
	 public static Potion him1;
	 public static Potion him2;
	 public static Potion him3;
	 public static Potion him4;
	 public static Potion psy1;
	 public static Potion psy2;
	 public static Potion psy3;
	 public static Potion psy4;
	 public static Potion rad1;
	 public static Potion rad2;
	 public static Potion rad3;
	 public static Potion rad4;
	 public static Potion radp1;
	 public static Potion radp2;
	 public static Potion radp3;
	 public static Potion radp4;
	 public static Potion himp1;
	 public static Potion himp2;
	 public static Potion himp3;
	 public static Potion himp4;	 
	 public static Potion psyp1;
	 public static Potion psyp2;
	 public static Potion psyp3;
	 public static Potion psyp4;	 
	 public static Potion brv1;
	 public static Potion brv2;
	 public static Potion brv3;
	 public static Potion brv4;
	 public static Potion electr1;
	 public static Potion electr2;
	 public static Potion electr3;
	 public static Potion electr4;
	 public static Potion electrp1;
	 public static Potion electrp2;
	 public static Potion electrp3;
	 public static Potion electrp4;
	 public static Potion termo1;
	 public static Potion termo2;
	 public static Potion termo3;
	 public static Potion termo4;
	 public static Potion termop1;
	 public static Potion termop2;
	 public static Potion termop3;
	 public static Potion termop4;
	 
	   private ArrayList mobsToDelete = new ArrayList();
	   private ArrayList creaturesToDelete = new ArrayList();
	   
	   
///////////////////////////////////////////////////////////////////////////Effects//////////////////////////////////////////////////////////////////////////////////////////////////

	 
///////////////////////////////////////////////////////////////////////////Artefacts////////////////////////////////////////////////////////////////////////////////////////////////
	 public static Item batareika = new batareika(2107).setUnlocalizedName("batareika");
	 public static Item bogon = new bogon(2108).setUnlocalizedName("bogon");
	 public static Item busi = new busi(2109).setUnlocalizedName("busi");
	 public static Item dusha = new dusha(2110).setUnlocalizedName("dusha");
	 public static Item ej = new ej(2111).setUnlocalizedName("ej");
	 public static Item glaz = new glaz(2112).setUnlocalizedName("glaz");
	 public static Item gravi = new gravi(2113).setUnlocalizedName("gravi");
	 public static Item sir = new sir(2114).setUnlocalizedName("sir");
	 public static Item kapli = new kapli(2115).setUnlocalizedName("kapli");
	 public static Item kolobok = new kolobok(2116).setUnlocalizedName("kolobok");
	 public static Item koluchka = new koluchka(2117).setUnlocalizedName("koluchka");
	 public static Item kompas = new kompas(2118).setUnlocalizedName("kompas");
	 public static Item kristall = new kristall(2119).setUnlocalizedName("kristall");
	 public static Item krovk = new krovk(2120).setUnlocalizedName("krovk");
	 public static Item lomot = new lomot(2121).setUnlocalizedName("lomot");
	 public static Item luna = new luna(2122).setUnlocalizedName("luna");
	 public static Item medusa = new medusa(2123).setUnlocalizedName("medusa");
	 public static Item nzvez = new nzvez(2124).setUnlocalizedName("nzvez");
	 public static Item oazis = new oazis(2125).setUnlocalizedName("oazis");
	 public static Item plamya = new plamya(2126).setUnlocalizedName("plamya");
	 public static Item plenka = new plenka(2127).setUnlocalizedName("plenka");
	 public static Item pruj = new pruj(2128).setUnlocalizedName("pruj");
	 public static Item pust = new pust(2129).setUnlocalizedName("pust");
	 public static Item puzir = new puzir(2130).setUnlocalizedName("puzir");
	 public static Item shar = new shar(2131).setUnlocalizedName("shar");
	 public static Item sliznyak = new sliznyak(2132).setUnlocalizedName("sliznyak");
	 public static Item sluda = new sluda(2133).setUnlocalizedName("sluda");
	 public static Item snej = new snej(2134).setUnlocalizedName("snej");
	 public static Item svet = new svet(2135).setUnlocalizedName("svet");
	 public static Item vivert = new vivert(2136).setUnlocalizedName("vivert");
	 public static Item vspishka = new vspishka(2137).setUnlocalizedName("vspishka");
	 public static Item zolribka = new zolribka(2138).setUnlocalizedName("zolribka");
	 public static Item zvetok = new zvetok(2139).setUnlocalizedName("zvetok");
	 public static Item zvezda = new zvezda(2140).setUnlocalizedName("zvezda");
///////////////////////////////////////////////////////////////////////////Artefacts////////////////////////////////////////////////////////////////////////////////////////////////
	 
///////////////////////////////////////////////////////////////////////////Medkits////////////////////////////////////////////////////////////////////////////////////////////////
	 public static Item anabiotic1 = new anabiotic1(2072, 0, -1.0F, false).setUnlocalizedName("anabiotic1");
	 public static Item antihim1 = new antihim1(2073, 0, -1.0F, false).setUnlocalizedName("antihim1"); 
	 public static Item antihim2 = new antihim2(2074, 0, -1.0F, false).setUnlocalizedName("antihim2");
	 public static Item antihim3 = new antihim3(2075, 0, -1.0F, false).setUnlocalizedName("antihim3");
	 public static Item antihim4 = new antihim4(2076, 0, -1.0F, false).setUnlocalizedName("antihim4");
	 public static Item antipsi1 = new antipsi1(2077, 0, -1.0F, false).setUnlocalizedName("antipsi1");
	 public static Item antipsi2 = new antipsi2(2078, 0, -1.0F, false).setUnlocalizedName("antipsi2"); 
	 public static Item antipsi3 = new antipsi3(2079, 0, -1.0F, false).setUnlocalizedName("antipsi3"); 
	 public static Item antipsi4 = new antipsi4(2080, 0, -1.0F, false).setUnlocalizedName("antipsi5"); 
	 public static Item antirad1 = new antirad1(2081, 0, -1.0F, false).setUnlocalizedName("antirad1"); 
	 public static Item antirad2 = new antirad2(2082, 0, -1.0F, false).setUnlocalizedName("antirad2"); 
	 public static Item antirad3 = new antirad3(2083, 0, -1.0F, false).setUnlocalizedName("antirad3"); 
	 public static Item antirad4 = new antirad4(2084, 0, -1.0F, false).setUnlocalizedName("antirad4"); 
	 public static Item apt1 = new apt1(2085, 0, -1.0F, false).setUnlocalizedName("apt1"); 
	 public static Item apt2 = new apt2(2086, 0, -1.0F, false).setUnlocalizedName("apt2"); 
	 public static Item apt3 = new apt3(2087, 0, -1.0F, false).setUnlocalizedName("apt3"); 
	 public static Item apt4 = new apt4(2088, 0, -1.0F, false).setUnlocalizedName("apt4"); 
	 public static Item apt5 = new apt5(2089, 0, -1.0F, false).setUnlocalizedName("apt5"); 
	 public static Item apt6 = new apt6(2090, 0, -1.0F, false).setUnlocalizedName("apt6"); 
	 public static Item apt7 = new apt7(2091, 0, -1.0F, false).setUnlocalizedName("apt7"); 
	 public static Item barv1 = new barv1(2092, 0, -1.0F, false).setUnlocalizedName("barv1"); 
	 public static Item barv2 = new barv2(2093, 0, -1.0F, false).setUnlocalizedName("barv2"); 
	 public static Item barv3 = new barv3(2094, 0, -1.0F, false).setUnlocalizedName("barv3"); 
	 public static Item barv4 = new barv4(2095, 0, -1.0F, false).setUnlocalizedName("barv4"); 
	 public static Item bint1 = new bint1(2096, 0, -1.0F, false).setUnlocalizedName("bint1"); 
	 public static Item bint2 = new bint2(2097, 0, -1.0F, false).setUnlocalizedName("bint2"); 
	 public static Item bint3 = new bint3(2098, 0, -1.0F, false).setUnlocalizedName("bint3"); 
	 public static Item bint4 = new bint4(2099, 0, -1.0F, false).setUnlocalizedName("bint4"); 
	 public static Item golova1 = new golova1(2100, 0, -1.0F, false).setUnlocalizedName("golova1"); 
	 public static Item hercules1 = new hercules1(2101, 0, -1.0F, false).setUnlocalizedName("hercules1"); 
	 public static Item hercules2 = new hercules2(2102, 0, -1.0F, false).setUnlocalizedName("hercules2"); 
	 public static Item rp1 = new rp1(2103, 0, -1.0F, false).setUnlocalizedName("rp1"); 
	 public static Item rp2 = new rp2(2104, 0, -1.0F, false).setUnlocalizedName("rp2"); 
	 public static Item rp3 = new rp3(2105, 0, -1.0F, false).setUnlocalizedName("rp3"); 
	 public static Item rp4 = new rp4(2106, 0, -1.0F, false).setUnlocalizedName("rp4"); 
///////////////////////////////////////////////////////////////////////////Medkits////////////////////////////////////////////////////////////////////////////////////////////////

	
	    public static void log(Object arg0){
	   	System.out.println("[STALMINE MERCURY] " + arg0);
	    }
	 
	    @EventHandler
	    public void postInit(FMLPostInitializationEvent event)
	    {
	    	
	    }
	    
	    @EventHandler
	    public void serverStart(FMLServerStartingEvent event) throws Exception {
	    	  this.mobsToDelete.add(EntitySpider.class);
	          this.mobsToDelete.add(EntitySkeleton.class);
	          this.mobsToDelete.add(EntityCreeper.class);
	          this.mobsToDelete.add(EntitySlime.class);
	          this.mobsToDelete.add(EntityEnderman.class);
	          this.creaturesToDelete.add(EntitySheep.class);
	          this.creaturesToDelete.add(EntityPig.class);
	          this.creaturesToDelete.add(EntityChicken.class);
	          this.creaturesToDelete.add(EntityHorse.class);
	          this.creaturesToDelete.add(EntityCow.class);
	          this.mobsToDelete.add(EntityZombie.class);
	          
	          BiomeGenBase[] arr$ = BiomeGenBase.biomeList;
	          int len$ = arr$.length;

	          for(int i$ = 0; i$ < len$; ++i$) {
	             BiomeGenBase biome = arr$[i$];
	             if(biome != null) {
	                Iterator spawns = biome.spawnableMonsterList.iterator();

	                while(spawns.hasNext()) {
	                   if(this.mobsToDelete.contains(((SpawnListEntry)spawns.next()).entityClass)) {
	                      spawns.remove();
	                   }
	                }

	                spawns = biome.spawnableCreatureList.iterator();

	                while(spawns.hasNext()) {
	                   if(this.creaturesToDelete.contains(((SpawnListEntry)spawns.next()).entityClass)) {
	                      spawns.remove();
	                   }
	                }

	              
	             }
	          }
	    }
	    
	    @EventHandler
	    public void init(FMLInitializationEvent event)
	    {
	    	
	    	
	    	
///////////////////////////////////////////////////////////////////////////Yazik//////////////////////////////////////////////////////////////////////////////////////////////////
	        LanguageRegistry.addName(alpen1, "Шоколад 'Альпен Голд'");
	        LanguageRegistry.addName(alpen2, "Шоколад 'Альпен Голд'");
	        LanguageRegistry.addName(alpen3, "Шоколад 'Альпен Голд'");
	        LanguageRegistry.addName(ananas, "Фрукт 'Ананас'");
	        LanguageRegistry.addName(assorti, "Консерва 'Ассорти'");
	        LanguageRegistry.addName(babana, "Фрукт 'Банан'");
	        LanguageRegistry.addName(bonduelle, "Консерва 'Бондюэль'");
	        LanguageRegistry.addName(bonpari1, "Мармелад 'Бон-Пари'");
	        LanguageRegistry.addName(bonpari2, "Мармелад 'Бон-Пари'");
	        LanguageRegistry.addName(bonpari3, "Мармелад 'Бон-Пари'");
	        LanguageRegistry.addName(bonpari4, "Мармелад 'Бон-Пари'");
	        LanguageRegistry.addName(file1, "Консерва 'Филе'");
	        LanguageRegistry.addName(file2, "Консерва 'Филе'");
	        LanguageRegistry.addName(file3, "Консерва 'Филе'");
	        LanguageRegistry.addName(file4, "Консерва 'Филе'");
	        LanguageRegistry.addName(file5, "Консерва 'Филе'");
	        LanguageRegistry.addName(file6, "Консерва 'Филе'");
	        LanguageRegistry.addName(flyaga, "Фляга");
	        LanguageRegistry.addName(govyadina, "Конесерва 'Говядина'");
	        LanguageRegistry.addName(hienz, "Консерва 'Хайенз'");
	        LanguageRegistry.addName(hleb1, "Черный Хлеб");
	        LanguageRegistry.addName(hleb2, "Белый Хлеб");
	        LanguageRegistry.addName(kilka, "Конесрва 'Килька'");
	        LanguageRegistry.addName(kolbosa1, "Колбаса 'Докторская'");
	        LanguageRegistry.addName(kolbosa2, "Колбаса 'Копченная'");
	        LanguageRegistry.addName(konyak, "Коньяк");
	        LanguageRegistry.addName(mandarin, "Фрукт 'Мандарин'");
	        LanguageRegistry.addName(mre, "Армейский Сухой Паек");
	        LanguageRegistry.addName(myaso, "Мясо");
	        LanguageRegistry.addName(ogyrci, "Огурцы 'Маринованные'");
	        LanguageRegistry.addName(oreo, "Печенье 'Орео'");
	        LanguageRegistry.addName(oreobig, "Печенье 'Орео'");
	        LanguageRegistry.addName(pechenie1, "Печенье 'Утреннее'");
	        LanguageRegistry.addName(pizza, "Пицца");
	        LanguageRegistry.addName(redbull, "Напиток 'Редбулл'");
	        LanguageRegistry.addName(rolton1, "Ролтон");
	        LanguageRegistry.addName(rolton2, "Ролтон");
	        LanguageRegistry.addName(saira, "Консерва 'Сайра'");
	        LanguageRegistry.addName(sendwich, "Сендвич");
	        LanguageRegistry.addName(shproti, "Консерва 'Шпроты'");
	        LanguageRegistry.addName(sok, "Сок 'Добрый'");
	        LanguageRegistry.addName(spam, "Консерва 'СПАМ'");
	        LanguageRegistry.addName(sufle1, "Консерва 'Суфле'");
	        LanguageRegistry.addName(sufle2, "Консерва 'Суфле'");
	        LanguageRegistry.addName(sufle3, "Консерва 'Суфле'");
	        LanguageRegistry.addName(sufle4, "Консерва 'Суфле'");
	        LanguageRegistry.addName(tynec, "Консерва 'Тунец'");
	        LanguageRegistry.addName(voda, "Вода 'Питьевая'");
	        LanguageRegistry.addName(vodka, "Водка 'Столичная'");
	        LanguageRegistry.addName(yabloko, "Яблоко");
	        LanguageRegistry.addName(bandit, "Нашивка 'Бандита'");
	        LanguageRegistry.addName(dolg, "Нашивка 'Долговца'");
	        LanguageRegistry.addName(military, "Нашивка 'Военного'");
	        LanguageRegistry.addName(monolith, "Нашивка 'Монолитовца'");
	        LanguageRegistry.addName(naemnik, "Нашивка 'Наемника'");
	        LanguageRegistry.addName(stalker, "Нашивка 'Сталкера'");
	        LanguageRegistry.addName(svoboda, "Нашивка 'Свободовца'");
	        LanguageRegistry.addName(comp, "Ноутбук");
	        LanguageRegistry.addName(rub10, "Купюра '10 рублей'");
	        LanguageRegistry.addName(rub100, "Купюра '100 рублей'");
	        LanguageRegistry.addName(rub1000, "Купюра '1000 рублей'");
	        LanguageRegistry.addName(rub100s, "Купюра '100 рублей' СОЧИ");
	        LanguageRegistry.addName(rub50, "Купюра '50 рублей'");
	        LanguageRegistry.addName(rub500, "Купюра '500 рублей'");
	        LanguageRegistry.addName(rub5000, "Купюра '5000 рублей'");
	        LanguageRegistry.addName(rub1, "Монета '1 рубль'");
	        LanguageRegistry.addName(rub2, "Монета '2 рубля'");
	        LanguageRegistry.addName(rub5, "Монета '5 рублей'");
	        LanguageRegistry.instance().addStringLocalization("potion.blood1", "Кровотечение 1");
	        LanguageRegistry.instance().addStringLocalization("potion.blood2", "Кровотечение 2");
	        LanguageRegistry.instance().addStringLocalization("potion.blood3", "Кровотечение 3");
	        LanguageRegistry.instance().addStringLocalization("potion.blood4", "Кровотечение 4");
	        LanguageRegistry.instance().addStringLocalization("potion.rad1", "Радиация 1");
	        LanguageRegistry.instance().addStringLocalization("potion.rad2", "Радиация 2");
	        LanguageRegistry.instance().addStringLocalization("potion.rad3", "Радиация 3");
	        LanguageRegistry.instance().addStringLocalization("potion.rad4", "Радиация 4");
	        LanguageRegistry.instance().addStringLocalization("potion.him1", "Химическое заражение 1");
	        LanguageRegistry.instance().addStringLocalization("potion.him2", "Химическое заражение 2");
	        LanguageRegistry.instance().addStringLocalization("potion.him3", "Химическое заражение 3");
	        LanguageRegistry.instance().addStringLocalization("potion.him4", "Химическое заражение 4");
	        LanguageRegistry.instance().addStringLocalization("potion.psy1", "Пси-излучение 1");
	        LanguageRegistry.instance().addStringLocalization("potion.psy2", "Пси-излучение 2");
	        LanguageRegistry.instance().addStringLocalization("potion.psy3", "Пси-излучение 3");
	        LanguageRegistry.instance().addStringLocalization("potion.psy4", "Пси-излучение 4");
	        LanguageRegistry.instance().addStringLocalization("potion.psyp1", "Пси-протектор 1");
	        LanguageRegistry.instance().addStringLocalization("potion.psyp2", "Пси-протектор 2");
	        LanguageRegistry.instance().addStringLocalization("potion.psyp3", "Пси-протектор 3");
	        LanguageRegistry.instance().addStringLocalization("potion.psyp4", "Пси-протектор 4");
	        LanguageRegistry.instance().addStringLocalization("potion.himp1", "Хим-протектор 1");
	        LanguageRegistry.instance().addStringLocalization("potion.himp2", "Хим-протектор 2");
	        LanguageRegistry.instance().addStringLocalization("potion.himp3", "Хим-протектор 3");
	        LanguageRegistry.instance().addStringLocalization("potion.himp4", "Хим-протектор 4");
	        LanguageRegistry.instance().addStringLocalization("potion.brv1", "Свертываемость 1");
	        LanguageRegistry.instance().addStringLocalization("potion.brv2", "Свертываемость 2");
	        LanguageRegistry.instance().addStringLocalization("potion.brv3", "Свертываемость 3");
	        LanguageRegistry.instance().addStringLocalization("potion.brv4", "Свертываемость 4");
	        LanguageRegistry.instance().addStringLocalization("potion.radp1", "Рад-протектор 1");
	        LanguageRegistry.instance().addStringLocalization("potion.radp2", "Рад-протектор 2");
	        LanguageRegistry.instance().addStringLocalization("potion.radp3", "Рад-протектор 3");
	        LanguageRegistry.instance().addStringLocalization("potion.radp4", "Рад-протектор 4");
	        LanguageRegistry.instance().addStringLocalization("potion.electr1", "Электричество 1");
	        LanguageRegistry.instance().addStringLocalization("potion.electr2", "Электричество 2");
	        LanguageRegistry.instance().addStringLocalization("potion.electr3", "Электричество 3");
	        LanguageRegistry.instance().addStringLocalization("potion.electr4", "Электричество 4");
	        LanguageRegistry.instance().addStringLocalization("potion.electrp1", "Вольт-протектор 1");
	        LanguageRegistry.instance().addStringLocalization("potion.electrp2", "Вольт-протектор 2");
	        LanguageRegistry.instance().addStringLocalization("potion.electrp3", "Вольт-протектор 3");
	        LanguageRegistry.instance().addStringLocalization("potion.electrp4", "Вольт-протектор 4");
	        LanguageRegistry.addName(anabiotic1, "Анабиотик 'Сталкерский'");
	        LanguageRegistry.addName(antihim1, "Антидот 'Сталкерский'");
	        LanguageRegistry.addName(antihim2, "Антидот 'Армейский'");
	        LanguageRegistry.addName(antihim3, "Антидот 'Научный'");
	        LanguageRegistry.addName(antihim4, "Антидот 'Особый'");
	        LanguageRegistry.addName(antipsi1, "Психоделин 'Сталкерский'");
	        LanguageRegistry.addName(antipsi2, "Психоделин 'Армейский'");
	        LanguageRegistry.addName(antipsi3, "Психоделин 'Научный'");
	        LanguageRegistry.addName(antipsi4, "Психоделин 'Особый'");
	        LanguageRegistry.addName(antirad1, "Антирад 'Сталкерский'");
	        LanguageRegistry.addName(antirad2, "Антирад 'Армейский'");
	        LanguageRegistry.addName(antirad3, "Антирад 'Научный'");
	        LanguageRegistry.addName(antirad4, "Антирад 'Особый'");
	        LanguageRegistry.addName(apt1, "Аптечка 'Сталкерская'");
	        LanguageRegistry.addName(apt2, "Аптечка 'Армейская'");
	        LanguageRegistry.addName(apt3, "Аптечка 'Научная'");
	        LanguageRegistry.addName(apt4, "Аптечка 'Особая'");
	        LanguageRegistry.addName(apt5, "Аптечка 'Эксперементальная'");
	        LanguageRegistry.addName(apt6, "Аптечка 'Аномальная'");
	        LanguageRegistry.addName(apt7, "Аптечка 'Дефицитная'");
	        LanguageRegistry.addName(barv1, "Барвинок 'Сталкерский'");
	        LanguageRegistry.addName(barv2, "Барвинок 'Армейский'");
	        LanguageRegistry.addName(barv3, "Барвинок 'Научный'");
	        LanguageRegistry.addName(barv4, "Барвинок 'Особый'");
	        LanguageRegistry.addName(bint1, "Бинт 'Сталкерский'");
	        LanguageRegistry.addName(bint2, "Бинт 'Армейский'");
	        LanguageRegistry.addName(bint3, "Бинт 'Научный'");
	        LanguageRegistry.addName(bint4, "Бинт 'Особый'");
	        LanguageRegistry.addName(golova1, "Цитрамон 'Сталкерский'");
	        LanguageRegistry.addName(hercules1, "Геркулес 'Сталкерский'");
	        LanguageRegistry.addName(hercules2, "Геркулес 'Армейский'");
	        LanguageRegistry.addName(rp1, "Радпротектор 'Сталкерский'");
	        LanguageRegistry.addName(rp2, "Радпротектор 'Армейский'");
	        LanguageRegistry.addName(rp3, "Радпротектор 'Научный'");
	        LanguageRegistry.addName(rp4, "Радпротектор 'Особый'");
	        LanguageRegistry.addName(batareika, "Артефакт 'Батарейка'");
	        LanguageRegistry.addName(bogon, "Артефакт 'Бенгальский огонь'");
	        LanguageRegistry.addName(busi, "Артефакт 'Мамины бусы'");
	        LanguageRegistry.addName(dusha, "Артефакт 'Душа'");
	        LanguageRegistry.addName(ej, "Артефакт 'Морской ёж'");
	        LanguageRegistry.addName(glaz, "Артефакт 'Глаз'");
	        LanguageRegistry.addName(gravi, "Артефакт 'Грави'");
	        LanguageRegistry.addName(sir, "Артефакт 'Плавленный сыр'");
	        LanguageRegistry.addName(kapli, "Артефакт 'Капля'");
	        LanguageRegistry.addName(kolobok, "Артефакт 'Колобок'");
	        LanguageRegistry.addName(koluchka, "Артефакт 'Колючка'");
	        LanguageRegistry.addName(kompas, "Артефакт 'Компасс'");
	        LanguageRegistry.addName(kristall, "Артефакт 'Кристалл'");
	        LanguageRegistry.addName(krovk, "Артефакт 'Кровь камня'");
	        LanguageRegistry.addName(lomot, "Артефакт 'Ломоть мяса'");
	        LanguageRegistry.addName(luna, "Артефакт 'Луна'");
	        LanguageRegistry.addName(medusa, "Артефакт 'Медуза'");
	        LanguageRegistry.addName(nzvez, "Артефакт 'Ночная звезда'");
	        LanguageRegistry.addName(oazis, "Артефакт 'Оазис'");
	        LanguageRegistry.addName(plamya, "Артефакт 'Пламя'");
	        LanguageRegistry.addName(plenka, "Артефакт 'Пленка'");
	        LanguageRegistry.addName(pruj, "Артефакт 'Пружина'");
	        LanguageRegistry.addName(pust, "Артефакт 'Пустышка'");
	        LanguageRegistry.addName(puzir, "Артефакт 'Пузырь'");
	        LanguageRegistry.addName(shar, "Артефакт 'Огненный шар'");
	        LanguageRegistry.addName(sliznyak, "Артефакт 'Слизняк'");
	        LanguageRegistry.addName(sluda, "Артефакт 'Слюда'");
	        LanguageRegistry.addName(snej, "Артефакт 'Снежинка'");
	        LanguageRegistry.addName(svet, "Артефакт 'Светляк'");
	        LanguageRegistry.addName(vivert, "Артефакт 'Выверт'");
	        LanguageRegistry.addName(vspishka, "Артефакт 'Вспышка'");
	        LanguageRegistry.addName(zolribka, "Артефакт 'Золотая рыбка'");
	        LanguageRegistry.addName(zvetok, "Артефакт 'Каменный цветок'");
	        LanguageRegistry.addName(zvezda, "Артефакт 'Звезда'");
	        
	       
	        
	        
///////////////////////////////////////////////////////////////////////////Yazik//////////////////////////////////////////////////////////////////////////////////////////////////
	        
	        
///////////////////////////////////////////////////////////////////////////Events//////////////////////////////////////////////////////////////////////////////////////////////////
		       MinecraftForge.EVENT_BUS.register(new eventblood1());
		       MinecraftForge.EVENT_BUS.register(new eventblood2());
		       MinecraftForge.EVENT_BUS.register(new eventblood3());
		       MinecraftForge.EVENT_BUS.register(new eventblood4()); 
		       MinecraftForge.EVENT_BUS.register(new eventrad1()); 
		       MinecraftForge.EVENT_BUS.register(new eventrad2()); 
		       MinecraftForge.EVENT_BUS.register(new eventrad3()); 
		       MinecraftForge.EVENT_BUS.register(new eventrad4()); 
		       MinecraftForge.EVENT_BUS.register(new eventpsy1()); 
		       MinecraftForge.EVENT_BUS.register(new eventpsy2());
		       MinecraftForge.EVENT_BUS.register(new eventpsy3());
		       MinecraftForge.EVENT_BUS.register(new eventpsy4()); 
		       MinecraftForge.EVENT_BUS.register(new eventhim1()); 
		       MinecraftForge.EVENT_BUS.register(new eventhim2()); 
		       MinecraftForge.EVENT_BUS.register(new eventhim3()); 
		       MinecraftForge.EVENT_BUS.register(new eventhim4()); 
		       MinecraftForge.EVENT_BUS.register(new eventhimp1()); 
		       MinecraftForge.EVENT_BUS.register(new eventhimp2()); 
		       MinecraftForge.EVENT_BUS.register(new eventhimp3()); 
		       MinecraftForge.EVENT_BUS.register(new eventhimp4());
		       MinecraftForge.EVENT_BUS.register(new eventradp1()); 
		       MinecraftForge.EVENT_BUS.register(new eventradp2()); 
		       MinecraftForge.EVENT_BUS.register(new eventradp3()); 
		       MinecraftForge.EVENT_BUS.register(new eventradp4());
		       MinecraftForge.EVENT_BUS.register(new eventpsyp1()); 
		       MinecraftForge.EVENT_BUS.register(new eventpsyp2()); 
		       MinecraftForge.EVENT_BUS.register(new eventpsyp3()); 
		       MinecraftForge.EVENT_BUS.register(new eventpsyp4()); 
		       MinecraftForge.EVENT_BUS.register(new eventbrv1()); 
		       MinecraftForge.EVENT_BUS.register(new eventbrv2()); 
		       MinecraftForge.EVENT_BUS.register(new eventbrv3()); 
		       MinecraftForge.EVENT_BUS.register(new eventbrv4()); 
		       MinecraftForge.EVENT_BUS.register(new eventelectr1()); 
		       MinecraftForge.EVENT_BUS.register(new eventelectr2()); 
		       MinecraftForge.EVENT_BUS.register(new eventelectr3()); 
		       MinecraftForge.EVENT_BUS.register(new eventelectr4());
		       MinecraftForge.EVENT_BUS.register(new eventelectrp1()); 
		       MinecraftForge.EVENT_BUS.register(new eventelectrp2()); 
		       MinecraftForge.EVENT_BUS.register(new eventelectrp3()); 
		       MinecraftForge.EVENT_BUS.register(new eventelectrp4());
		       MinecraftForge.EVENT_BUS.register(new eventtermo1()); 
		       MinecraftForge.EVENT_BUS.register(new eventtermo2()); 
		       MinecraftForge.EVENT_BUS.register(new eventtermo3()); 
		       MinecraftForge.EVENT_BUS.register(new eventtermo4());
		       MinecraftForge.EVENT_BUS.register(new eventtermop1()); 
		       MinecraftForge.EVENT_BUS.register(new eventtermop2()); 
		       MinecraftForge.EVENT_BUS.register(new eventtermop3()); 
		       MinecraftForge.EVENT_BUS.register(new eventtermop4());
///////////////////////////////////////////////////////////////////////////Events//////////////////////////////////////////////////////////////////////////////////////////////////
		       
		       
///////////////////////////////////////////////////////////////////////////Effects//////////////////////////////////////////////////////////////////////////////////////////////////
		       blood1 = new blood1(31, false, 0).setIconIndex(2, 2).setPotionName("potion.blood1"); 
		       blood2 = new blood2(32, false, 0).setIconIndex(2, 2).setPotionName("potion.blood2"); 
		       blood3 = new blood3(33, false, 0).setIconIndex(2, 2).setPotionName("potion.blood3"); 
		       blood4 = new blood4(34, false, 0).setIconIndex(2, 2).setPotionName("potion.blood4"); 
		       rad1 = new rad1(35, false, 0).setIconIndex(3, 2).setPotionName("potion.rad1");  
		       rad2 = new rad2(36, false, 0).setIconIndex(3, 2).setPotionName("potion.rad2"); 
		       rad3 = new rad3(37, false, 0).setIconIndex(3, 2).setPotionName("potion.rad3"); 
		       rad4 = new rad4(38, false, 0).setIconIndex(3, 2).setPotionName("potion.rad4"); 
		       him1 = new him1(39, false, 0).setIconIndex(4, 2).setPotionName("potion.him1");   
		       him2 = new him2(40, false, 0).setIconIndex(4, 2).setPotionName("potion.him2"); 
		       him3 = new him3(41, false, 0).setIconIndex(4, 2).setPotionName("potion.him3"); 
		       him4 = new him4(42, false, 0).setIconIndex(4, 2).setPotionName("potion.him4"); 
		       psy1 = new psy1(43, false, 0).setIconIndex(5, 2).setPotionName("potion.psy1"); 
		       psy2 = new psy2(44, false, 0).setIconIndex(5, 2).setPotionName("potion.psy2"); 
		       psy3 = new psy3(45, false, 0).setIconIndex(5, 2).setPotionName("potion.psy3"); 
		       psy4 = new psy4(46, false, 0).setIconIndex(5, 2).setPotionName("potion.psy4"); 
		       psyp1 = new psy1(47, false, 0).setIconIndex(5, 2).setPotionName("potion.psyp1"); 
		       psyp2 = new psy2(48, false, 0).setIconIndex(5, 2).setPotionName("potion.psyp2"); 
		       psyp3 = new psy3(49, false, 0).setIconIndex(5, 2).setPotionName("potion.psyp3"); 
		       psyp4 = new psy4(50, false, 0).setIconIndex(5, 2).setPotionName("potion.psyp4"); 
		       himp1 = new him1(51, false, 0).setIconIndex(4, 2).setPotionName("potion.himp1");   
		       himp2 = new him2(52, false, 0).setIconIndex(4, 2).setPotionName("potion.himp2"); 
		       himp3 = new him3(53, false, 0).setIconIndex(4, 2).setPotionName("potion.himp3"); 
		       himp4 = new him4(54, false, 0).setIconIndex(4, 2).setPotionName("potion.himp4"); 
		       radp1 = new rad1(55, false, 0).setIconIndex(3, 2).setPotionName("potion.radp1");  
		       radp2 = new rad2(56, false, 0).setIconIndex(3, 2).setPotionName("potion.radp2"); 
		       radp3 = new rad3(57, false, 0).setIconIndex(3, 2).setPotionName("potion.radp3"); 
		       radp4 = new rad4(58, false, 0).setIconIndex(3, 2).setPotionName("potion.radp4"); 
		       brv1 = new blood1(59, false, 0).setIconIndex(2, 2).setPotionName("potion.brv1"); 
		       brv2 = new blood2(60, false, 0).setIconIndex(2, 2).setPotionName("potion.brv2"); 
		       brv3 = new blood3(61, false, 0).setIconIndex(2, 2).setPotionName("potion.brv3"); 
		       brv4 = new blood4(62, false, 0).setIconIndex(2, 2).setPotionName("potion.brv4");
		  	  electr1 = new electr1(63, false, 0).setIconIndex(2, 2).setPotionName("potion.electr1"); 
			  electr2 = new electr2(64, false, 0).setIconIndex(2, 2).setPotionName("potion.electr2"); 
			  electr3 = new electr3(65, false, 0).setIconIndex(2, 2).setPotionName("potion.electr3"); 
			  electr4 = new electr4(66, false, 0).setIconIndex(2, 2).setPotionName("potion.electr4"); 
			  electrp1 = new electrp1(67, false, 0).setIconIndex(2, 2).setPotionName("potion.electrp1"); 
			  electrp2 = new electrp2(68, false, 0).setIconIndex(2, 2).setPotionName("potion.electrp2"); 
			  electrp3 = new electrp3(69, false, 0).setIconIndex(2, 2).setPotionName("potion.electrp3"); 
			  electrp4 = new electrp4(70, false, 0).setIconIndex(2, 2).setPotionName("potion.electrp4"); 
			  termo1 = new termo1(71, false, 0).setIconIndex(2, 2).setPotionName("potion.termo1"); 
			  termo2 = new termo2(72, false, 0).setIconIndex(2, 2).setPotionName("potion.termo2"); 
			  termo3 = new termo3(73, false, 0).setIconIndex(2, 2).setPotionName("potion.termo3"); 
			  termo4 = new termo4(74, false, 0).setIconIndex(2, 2).setPotionName("potion.termo4"); 
			  termop1 = new termop1(75, false, 0).setIconIndex(2, 2).setPotionName("potion.termop1"); 
			  termop2 = new termop2(76, false, 0).setIconIndex(2, 2).setPotionName("potion.termop2"); 
			  termop3 = new termop3(77, false, 0).setIconIndex(2, 2).setPotionName("potion.termop3"); 
			  termop4 = new termop4(78, false, 0).setIconIndex(2, 2).setPotionName("potion.termop4"); 
///////////////////////////////////////////////////////////////////////////Effects//////////////////////////////////////////////////////////////////////////////////////////////////
}
	
///////////////////////////////////////////////////////////////////////////POTION////////////////////////////////////////////////////////////////////////////////////////////////
	       @EventHandler
	       public void preinit(FMLPreInitializationEvent event)
	       {
	           Potion[] potionTypes = null;

	           for (Field f : Potion.class.getDeclaredFields()) {
	               f.setAccessible(true);
	               try {
	                   if (f.getName().equals("potionTypes") || f.getName().equals("field_76425_a")) {
	                       Field modfield = Field.class.getDeclaredField("modifiers");
	                       modfield.setAccessible(true);
	                       modfield.setInt(f, f.getModifiers() & ~Modifier.FINAL);

	                       potionTypes = (Potion[])f.get(null);
	                       final Potion[] newPotionTypes = new Potion[256];
	                       System.arraycopy(potionTypes, 0, newPotionTypes, 0, potionTypes.length);
	                       f.set(null, newPotionTypes);
	                   }
	               }
	               catch (Exception e) {
	                   System.err.println("Severe error, please report this to the mod author:");
	                   System.err.println(e);
	               }
	           }
	       }
	       
	       
	       @Mod.EventHandler
	       public void PreLoad(FMLPreInitializationEvent event){
	           proxy.initMod();
	           blockcore.loadBlocks();
	           anomalycore.loadAnom();

	       }
	       
	       
	       
///////////////////////////////////////////////////////////////////////////POTION////////////////////////////////////////////////////////////////////////////////////////////////      

	       
}
