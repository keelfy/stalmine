package net.sm.food;


import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.sm.tabs.registrtab;


public class sendwich extends ItemFood{
	
	
//REGISTR	
public sendwich(int par1, int par2, float par3, boolean par4)
{
super(par1, par2, par4);
maxStackSize = 64;
setCreativeTab(registrtab.tabfood);
}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "Обычный бутерброт из двух булочек, со-");
	 par3List.add(EnumChatFormatting.YELLOW + "уса, помидорки, листа капусты, соси-");
	 par3List.add(EnumChatFormatting.YELLOW + "ски, огурца, сыра и моркошки.Сталкеры");
	 par3List.add(EnumChatFormatting.YELLOW + "обожают побаловать себя изредка такой");
	 par3List.add(EnumChatFormatting.YELLOW + "едой, купив у барменов.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
     par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.DARK_PURPLE + " МИФИЧЕСКИЙ");
     par3List.add(EnumChatFormatting.BLUE + "Утоление голода: 12");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.4 КГ");
}



//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//EFFEKT


//IKONKA
@Override
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:food/sendwich");}

}