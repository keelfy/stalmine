package net.sm.food;


import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.sm.tabs.registrtab;


public class redbull extends ItemFood{
	
	
//REGISTR	
public redbull(int par1, int par2, float par3, boolean par4)
{
super(par1, par2, par4);
maxStackSize = 64;
setCreativeTab(registrtab.tabfood);
}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "Напиток, повышающий работоспособность");
	 par3List.add(EnumChatFormatting.YELLOW + "стимулируя внутренние резервы органи-");
	 par3List.add(EnumChatFormatting.YELLOW + "зма.В напитке присутсвует кофеин и та-");
	 par3List.add(EnumChatFormatting.YELLOW + "урин, что увеличивает запас энергии в");
	 par3List.add(EnumChatFormatting.YELLOW + "организме.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
     par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.DARK_AQUA + " РЕДКИЙ");
     par3List.add(EnumChatFormatting.BLUE + "Утоление жажды: 4");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.4 КГ");
}

public EnumAction getItemUseAction(ItemStack par1ItemStack)
{
    return EnumAction.drink;
}

//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//EFFEKT
protected void onFoodEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
{
     par3EntityPlayer.addPotionEffect(new PotionEffect(1, 600, 1));
}

//IKONKA
@Override
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:food/redbull");}

}