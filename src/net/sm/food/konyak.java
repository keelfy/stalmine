package net.sm.food;


import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.sm.tabs.registrtab;


public class konyak extends ItemFood{
	
	
//REGISTR	
public konyak(int par1, int par2, float par3, boolean par4)
{
super(par1, par2, par4);
maxStackSize = 64;
setCreativeTab(registrtab.tabfood);
}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "Коньяк это крепный алкогольный напи-");
	 par3List.add(EnumChatFormatting.YELLOW + "ток, подвид 'Бренди', производимый из");
	 par3List.add(EnumChatFormatting.YELLOW + "определенных сортов винограда.Торговцы");
	 par3List.add(EnumChatFormatting.YELLOW + "любят особые алкогольные товары, по-");
	 par3List.add(EnumChatFormatting.YELLOW + "этому платят не мало за них.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
     par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.GOLD + " БЕСЦЕННЫЙ");
     par3List.add(EnumChatFormatting.BLUE + "Утоление жажды: 4");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 1 КГ");
}

public EnumAction getItemUseAction(ItemStack par1ItemStack)
{
    return EnumAction.drink;
}

//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//EFFEKT
protected void onFoodEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
{
     par3EntityPlayer.addPotionEffect(new PotionEffect(9, 600, 0));
}

//IKONKA
@Override
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:food/konyak");}

}