package net.sm.food;


import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.sm.tabs.registrtab;


public class hleb1 extends ItemFood{
	
	
//REGISTR	
public hleb1(int par1, int par2, float par3, boolean par4)
{
super(par1, par2, par4);
maxStackSize = 64;
setCreativeTab(registrtab.tabfood);
}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "Ржаной хлеб, который уже привыкли");
	 par3List.add(EnumChatFormatting.YELLOW + "называть черным является обычным");
	 par3List.add(EnumChatFormatting.YELLOW + "продуктом средь всей Северной Ев-");
	 par3List.add(EnumChatFormatting.YELLOW + "ропы, а для Сталкеров это незаме-");
	 par3List.add(EnumChatFormatting.YELLOW + "нимая вещь в рюкзаке.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
     par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.GRAY +  " ОБЫЧНЫЙ");
     par3List.add(EnumChatFormatting.BLUE + "Утоление голода: 2");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.2 КГ");
}



//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//EFFEKT


//IKONKA
@Override
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:food/hleb1");}

}