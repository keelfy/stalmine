package net.sm.food;


import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.sm.tabs.registrtab;


public class flyaga extends ItemFood{
	
	
//REGISTR	
public flyaga(int par1, int par2, float par3, boolean par4)
{
super(par1, par2, par4);
maxStackSize = 1;
setCreativeTab(registrtab.tabfood);
}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "Фляжка для воды предназначена что-бы");
	 par3List.add(EnumChatFormatting.YELLOW + "набирать из колодца воду, и хранить");
	 par3List.add(EnumChatFormatting.YELLOW + "долгое время, а затем пить.Можно за-");
	 par3List.add(EnumChatFormatting.YELLOW + "ного заполнить.Незаменимая вещь для");
	 par3List.add(EnumChatFormatting.YELLOW + "Сталкеров.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
     par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.AQUA + " НЕОБЫЧНЫЙ");
	 par3List.add(EnumChatFormatting.BLUE + "Утоление жажды: 10");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.5 КГ");
}

public EnumAction getItemUseAction(ItemStack par1ItemStack)
{
    return EnumAction.drink;
}



//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//EFFEKT


//IKONKA
@Override
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:food/flyaga");}

}