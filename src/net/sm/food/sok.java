package net.sm.food;


import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.sm.tabs.registrtab;


public class sok extends ItemFood{
	
	
//REGISTR	
public sok(int par1, int par2, float par3, boolean par4)
{
super(par1, par2, par4);
maxStackSize = 64;
setCreativeTab(registrtab.tabfood);
}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "Свежевыжатый сок из множества фрук-");
	 par3List.add(EnumChatFormatting.YELLOW + "тов, обычный товар для сталкеров и");
	 par3List.add(EnumChatFormatting.YELLOW + "их приятелей.Уж приспичит попить- до");
	 par3List.add(EnumChatFormatting.YELLOW + "ставай сок из рюкзака и пей.Просто,");
	 par3List.add(EnumChatFormatting.YELLOW + "удобно, дешево.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
     par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.AQUA + " НЕОБЫЧНЫЙ");
     par3List.add(EnumChatFormatting.BLUE + "Утоление жажды: 6");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.3 КГ");
}

public EnumAction getItemUseAction(ItemStack par1ItemStack)
{
    return EnumAction.drink;
}

//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//EFFEKT


//IKONKA
@Override
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:food/sok");}

}