package net.sm.food;


import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.sm.tabs.registrtab;


public class bonpari1 extends ItemFood{
	
	
//REGISTR	
public bonpari1(int par1, int par2, float par3, boolean par4)
{
super(par1, par2, par4);
maxStackSize = 64;
setCreativeTab(registrtab.tabfood);
}


//OPISANIE
public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
{

	 par3List.add(EnumChatFormatting.YELLOW + "Мармелад жевательный 'Бон-Пари' пре-");
	 par3List.add(EnumChatFormatting.YELLOW + "дставляет собой мармелад в форме че-");
	 par3List.add(EnumChatFormatting.YELLOW + "рвячков, с добавлением витаминов C,");
	 par3List.add(EnumChatFormatting.YELLOW + "красителей и желатина.Отличное соче-");
	 par3List.add(EnumChatFormatting.YELLOW + "тание вкуса малины, яблока и лимона.");
	 par3List.add(EnumChatFormatting.GRAY + "-----------------------------------");
     par3List.add(EnumChatFormatting.BLUE + "Раритетность:"  + EnumChatFormatting.DARK_PURPLE + " МИФИЧЕСКИЙ");
	 par3List.add(EnumChatFormatting.BLUE + "Утоление голода: 3");
	 par3List.add(EnumChatFormatting.BLUE + "Вес: 0.1 КГ");
}



//EnumChatFormatting.GRAY + "ОБЫЧНЫЙ"
//EnumChatFormatting.AQUA + "НЕОБЫЧНЫЙ"
//EnumChatFormatting.DARK_AQUA + "РЕДКИЙ"
//EnumChatFormatting.DARK_PURPLE + "МИФИЧЕСКИЙ"
//EnumChatFormatting.LIGHT_PURPLE + "ЛЕГЕНДАРНЫЙ"
//EnumChatFormatting.GOLD + "БЕСЦЕННЫЙ"
//EnumChatFormatting.GREEN + "НЕВООБРАЗИМЫЙ"
//EnumChatFormatting.RED + "СПЕЦИАЛЬНЫЙ"


//EFFEKT


//IKONKA
@Override
public void registerIcons(IconRegister par1IconRegister){this.itemIcon = par1IconRegister.registerIcon("mercury:food/bonpari1");}

}